import java.util.*;
import java.util.HashMap;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
public class TP1{
    static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
    static BufferedWriter write = new BufferedWriter(new OutputStreamWriter(System.out));
    public static void main(String[]Args) throws IOException{
        
        int N = Integer.parseInt(read.readLine());                                                                      //N sebagai jumlah toko
        HashMap<String,Toko> tokolist = new HashMap<String,Toko>();
        int[] targets;                                                                                                  //Array array untuk menyimpan target,duar dan kemungkinan
        int[] duar;
        int[] possibilities;
        for(int x = 0;x < N ;x++){                                                                                      //Membuat toko toko dan donut donut di dalam nya
            String[] input = read.readLine().split(" ");
            String names = input[0];
            int types = Integer.parseInt(input[1]);
            Toko t = new Toko(names,types,false);
            tokolist.put(names,t);
        }
        int arraylength = 0;                                                                                           //membuat array array penyimpan stock dan  
        for(Object key : tokolist.keySet()){                                                                            //ChocoValue untuk digunakan di count.
            arraylength += tokolist.get(key).TypesofDonuts();                                                             //Disini, panjangnya ditentukan
        }
        int[] ChocoValue = new int[arraylength + 1];
        int[] Stocks = new int[arraylength + 1];
        
        int days = Integer.parseInt(read.readLine());                                                                //men set berapa hari. 
        possibilities = new int[days];                                                                              //menset juga berapa banyak kemungkinan yang harus dikeluarkan.
        targets = new int[days];
        duar = new int[days];
        for(Object key : tokolist.keySet()){                                                                       //ada array penyimpan hari di toko, for some reason.
            Toko a = tokolist.get(key);
            a.setDays(days);
        }
        for(int z = 0;z < days;z++){   
            for(Object key : tokolist.keySet()){
                Toko t = tokolist.get(key);                                                                         //me reset semua nilai di array chocovalue menjadi 0 
                for(Object key1 : t.donutdetail.keySet()){
                    if(t.donutdetail.get(key1).getStock() == 0){
                        t.donutdetail.remove(key1);
                    }
                }
                }                             
            for(int x = 0; x < ChocoValue.length;x++){                                                           
                ChocoValue[x] = 0; 
                Stocks[x] = 0; 
            }
        arraylength = 0;                                                                                           //membuat array array penyimpan stock dan  
        for(Object key : tokolist.keySet()){                                                                            //ChocoValue untuk digunakan di count.
            arraylength += tokolist.get(key).TypesofDonuts();                                                             //Disini, panjangnya ditentukan
        }
        ChocoValue = new int[arraylength + 1];
        Stocks = new int[arraylength + 1];
        
        for(Object Key : tokolist.keySet()){
            tokolist.get(Key).setOpen(false);
        }
            int openstoresnum = Integer.parseInt(read.readLine());                                                   // mengatur ada berapa store yang buka di hari h, 
            if(openstoresnum > 0){                      
                String[] openstores = read.readLine().split(" ");                                                       //dan nge set toko yang buka apa saja.
                for(int x = 0 ; x < openstoresnum;x++){
                    Toko y = tokolist.get(openstores[x]);
                    if(tokolist.containsKey(openstores[x])){
                        y.setOpen(true);
                    }
                }
            }
            targets[z] =Integer.parseInt(read.readLine().split(" ")[1]);                                        //men set target hari itu berapa.
            int counter = 0;
            for(Object Key : tokolist.keySet()){
                if(tokolist.get(Key).isOpen() == true){
                    if(tokolist.get(Key).TypesofDonuts() > 0){  
                        donut[] placeholder = tokolist.get(Key).asArray();
                        for(int x = 0; x < placeholder.length;x++){
                            ChocoValue[counter] = placeholder[x].findchoco();
                            Stocks[counter] = placeholder[x].getStock();
                            counter++;
                        }
                    }
                }   
            }
            possibilities[z] =  count(ChocoValue,Stocks,targets[z]);
            int duars = Integer.parseInt(read.readLine().split(" ")[1]);
            for(int d = 0; d < duars; d++){
                String[] explosions = read.readLine().split(" ");
                tokolist.get(explosions[0]).explosion(explosions[1],Integer.parseInt(explosions[2]));

            }
            String[] restock = read.readLine().split(" ");
            int restocknum = Integer.parseInt(restock[1]);
            for(int r = 0;r < restocknum;r++ ){
                String[] restockdetail = read.readLine().split(" ");
                Toko restocked = tokolist.get(restockdetail[0]);
                restocked.restock(restockdetail[1],Integer.parseInt(restockdetail[2]),Integer.parseInt(restockdetail[3]));
            }
            String[] transfer = read.readLine().split(" ");
            int transfernum = Integer.parseInt(transfer[1]);
            for(int t = 0; t < transfernum; t++){
                String[] transferdetail = read.readLine().split(" ");
                String from = transferdetail[0];
                String to = transferdetail[1];
                String Transferredonut = transferdetail[2];
                int Numberofdonuts = Integer.parseInt(transferdetail[3]);
                Toko dari = tokolist.get(from);
                Toko untuk = tokolist.get(to);
                donut x = dari.getDonut(Transferredonut);
                if(untuk.checkdonut(x.getName()) == true){
                    donut y = untuk.getDonut(x.getName());
                    if(x.getStock() > 0){
                        if(x.findchoco() == y.findchoco()){
                            if(x.getStock() >= Numberofdonuts){
                                dari.transfer(Transferredonut, Numberofdonuts);
                                untuk.receive(x, Numberofdonuts);
                            }

                    }
                }

           
            }
            else{
                if(x.getStock() >= Numberofdonuts){
                    dari.transfer(Transferredonut, Numberofdonuts);
                    untuk.receive(x, Numberofdonuts);
                }

            }
        }
    }
        for(int x = 0;x < possibilities.length; x++){
            System.out.println(possibilities[x]);
        }
    } 
    public static int count(int[] ChocoValue, int[] Stock ,int target) {
        int res = 0;

        if(target == 0){
            return 1;
        }
        else if (ChocoValue.length >= 1){
            int[] newchoco = new int[ChocoValue.length - 1];
            int[] newstocker = new int[Stock.length - 1];
            int counter = 0;
            for(int x = 1;x < Stock.length;x++){
                newchoco[counter] = ChocoValue[x];
                newstocker[counter] = Stock[x];
                counter++;
            }
            if(Stock[0] == 0){
                res += count(newchoco,newstocker,target);
            }
            else if (target < 0){
                return 0;
            }
            else{
                int[] newstock = new int[Stock.length];
                for(int x = 0; x < Stock.length; x++){
                    newstock[x] = Stock[x];
                }
                newstock[0] -= 1;
                int newtarget = target - ChocoValue[0];
                int temp1 = count(newchoco, newstocker, target);
                int temp2 = count(ChocoValue,newstock,newtarget);
                int temp3 = temp1 + temp2;
                res += temp3;
            }
        }
        else{
            return 0;
        }

        return (res%1000000007);
    }
    static class Toko{
        private String Name;
        private int donuttypes;
        private HashMap<String,donut> donutdetail = new HashMap<String,donut>();
        private Boolean[] status;
        private Boolean open;
    
        public Toko(String Name,int type,Boolean open) throws IOException{
            this.Name = Name;
            this.donuttypes = type;
            this.open = open;
            for(int x = 0;x < donuttypes;x++){
                String[] input = read.readLine().split(" ");
                String name = input[0];
                int stock = Integer.parseInt(input[1]);
                int chips = Integer.parseInt(input[2]);
                donut d = new donut(name,stock,chips);
                donutdetail.put(name,d); 
          }
        }
        public int TypesofDonuts(){
            return donutdetail.size();
        }
        public void setDays(int x){
            this.status = new Boolean[x];
        }
       public donut[] asArray(){
           return donutdetail.values().toArray(new donut[donutdetail.size()]);
       }
        public Boolean isOpen(){
            return this.open;
        }
        public void setOpen(Boolean value){
            this.open = value;
        }
        
        public String toString(){
            return this.Name;
        }
        public void restock(String Donutkey, int Donutnum, int Choconum){
                if(donutdetail.containsKey(Donutkey)){
                donut y = donutdetail.get(Donutkey);
                if(Choconum == y.findchoco()){
                    if(donutdetail.get(Donutkey).getStock() <= 0){
                        donutdetail.get(Donutkey).setChip(Choconum);
                        donutdetail.get(Donutkey).restock(Donutnum);
                    }
                    else{
                        y.restock(Donutnum);
                    }
                }

            }
            else{
                donut d = new donut(Donutkey,Donutnum,Choconum);
                donutdetail.put(Donutkey, d);
            }
        }
        public void explosion(String Donutkey, int Donutnum){
            donutdetail.get(Donutkey).duar(Donutnum);
            if(donutdetail.get(Donutkey).getStock() <= 0){
                donutdetail.remove(Donutkey);
            }
        }
        public void transfer(String Donutkey, int x){
            donutdetail.get(Donutkey).transfer(x);
            if(donutdetail.get(Donutkey).getStock() == 0){
                donutdetail.remove(Donutkey);
            }
        }
        public donut getDonut(String Donutkey){
            return donutdetail.get(Donutkey);
        }
        public void receive(donut Donuter, int x){
            String Donutkey = Donuter.getName();
            if(donutdetail.containsKey(Donutkey)){
                donutdetail.get(Donutkey).receive(Donuter, x);
            }
            else{
                donutdetail.put(Donutkey,Donuter);
                donutdetail.get(Donutkey).restock(x);
            }
            
        }
        public boolean checkdonut(String x){
            if(donutdetail.containsKey(x)){
                return true;
            }
            else{
                return false;
            }
        }
    }
    static class donut{
        private String name;
        private int stock;
        private int ChocoChips;
        
        public donut(String name, int stock, int ChocoChips){
            this.name = name;
            this.stock = stock;
            this.ChocoChips = ChocoChips;
        }
        public void restock(int x){
            this.stock += x;
        }
        public void transfer(int x){
            this.stock -= x;
            }

        
        public void receive (donut Donut, int z){
            if(Donut.ChocoChips == this.ChocoChips){
                stock += z;
            }

        }
        public int findchoco(){
            return this.ChocoChips;
        }
        public int getStock(){
            return this.stock;
        }
        public void duar(int y){
            if(stock >= 1){
            this.stock -= y;
            }

        }
        public String getName(){
            return this.name;
        }
        public String toString(){
           return Integer.toString(this.stock);
        }
        public void setChip(int x){
            this.ChocoChips = x;
        }
    }
}