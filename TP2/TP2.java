import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.*;
import java.io.IOException;

public class TP2{
    public static void main(String[] args) throws IOException{
        int[][] donutline;
        BufferedReader read = new BufferedReader(new InputStreamReader (System.in));
        int N = Integer.parseInt(read.readLine());
        donutline = new int[N][];
        for(int x = 0; x < N; x++){
            String[] donutlinestring = read.readLine().split(" ");
            int numdonuts = Integer.parseInt(donutlinestring[0]);
            int[] donutrow = new int[numdonuts];
            for(int y = 0; y < numdonuts ; y++){
                donutrow[y] = Integer.parseInt(donutlinestring[y + 1]);
            }
            donutline[x] = donutrow;
        }
        for(int sort = 0;sort < donutline.length; sort++){
            for(int sortee = sort + 1;sortee <donutline.length; sortee++){
                if(donutline[sort][0] > donutline[sortee][0]){
                    swap(donutline,sort,sortee);
                }
            }
        }
        // for(int kyo = 0; kyo < donutline.length;kyo++){
        //     System.out.println(Arrays.toString(donutline[kyo]));
        // }
        
        int Q = Integer.parseInt(read.readLine());   
        for(int z = 0; z < Q; z++){
            String[] command = read.readLine().split(" ");
            // System.out.println(command[0]);
            // System.out.println(command[0] == "IN_FRONT");
            if(command[0].equals("IN_FRONT")){
                int addee = Integer.parseInt(command[1]);
                int added = Integer.parseInt(command[2]) - 1;
                int[] newarray = new int[donutline[added].length + 1];
                newarray[0] = addee;
                for(int zo = 0; zo < donutline[added].length;zo++){
                    newarray[zo+1] = donutline[added][zo];
                }
                donutline[added] = newarray;
                sort(donutline);
                destroy(donutline);
            }
            else if(command[0].equals("OUT_FRONT")){
                int outed = Integer.parseInt(command[1]) - 1;
                int[] newarray = new int[donutline[outed].length - 1];
                for(int zo = 0;zo < donutline[outed].length - 1;zo++){
                    newarray[zo] = donutline[outed][zo+1];
                }
                donutline[outed] = newarray;
                sort(donutline);
                destroy(donutline);
            }
            else if(command[0].equals("IN_BACK")){
                int addee = Integer.parseInt(command[1]);
                int added = Integer.parseInt(command[2]) - 1;
                int[] newarray = new int[donutline[added].length + 1];
                newarray[donutline[added].length] = addee;
                for(int zo = 0; zo < donutline[added].length;zo++){
                    newarray[zo] = donutline[added][zo];
                }
                donutline[added] = newarray;
                sort(donutline);
                destroy(donutline);

            }
            else if(command[0].equals("OUT_BACK")){
                int outed = Integer.parseInt(command[1]) - 1;
                int[] newarray = new int[donutline[outed].length];
                for(int zo = 0;zo < donutline[outed].length - 1;zo++){
                    newarray[zo] = donutline[outed][zo+1];
                }
                donutline[outed] = newarray;
                sort(donutline);
                destroy(donutline);

            }
            else if(command[0].equals("MOVE_BACK")){
                int asal = Integer.parseInt(command[1]) - 1;
                int tujuan = Integer.parseInt(command[2]) - 1;
                int[] newarray = new int[donutline[asal].length + donutline[tujuan].length];
                for(int zo = 0; zo < donutline[tujuan].length;zo++){
                    newarray[zo] = donutline[tujuan][zo];
                }
                for(int zi = donutline[tujuan].length ; zi < donutline[asal].length + donutline[tujuan].length;zi++){
                    newarray[zi] = donutline[asal][zi - donutline[tujuan].length];
                }
                donutline[tujuan] = newarray;
                donutline[asal] = new int[1];
                donutline[asal][0] = 0;
                sort(donutline);
                destroy(donutline);


            }
            else if(command[0].equals("MOVE_FRONT")){
                int asal = Integer.parseInt(command[1]);
                int tujuan = Integer.parseInt(command[2]);
                int[] newarray = new int[donutline[asal].length + donutline[tujuan].length];
                for(int zo = 0; zo<donutline[asal].length;zo++){
                    newarray[zo] = donutline[asal][zo];
                }
                for(int zi = donutline[asal].length ; zi<donutline[asal].length + donutline[tujuan].length;zi++){
                    newarray[zi] = donutline[tujuan][zi - donutline[asal].length];
                }
                donutline[tujuan] = newarray;
                donutline[asal] = new int[1];
                donutline[asal][0] = 0;
                sort(donutline);
                destroy(donutline);

            }
            else if(command[0].equals("NEW")){
                int[][] newarray = new int[donutline.length + 1][];
                int[] masukan = new int[1];
                masukan[0] = Integer.parseInt(command[1]);
                for(int zo = 0; zo < donutline.length; zo++){
                    newarray[zo] = donutline[zo];
                }
                newarray[donutline.length] = masukan;
                donutline = newarray;
                sort(donutline);
                destroy(donutline);

            }
            //MENGAHPUS ARRAY YANG ISINYA HANYA 0
            //MENSORT BAGIAN DEPAN ARRAY DI DONUTLINE
        }
        for(int kyo = 0; kyo < donutline.length;kyo++){
            System.out.println(Arrays.toString(donutline[kyo]));
        }
    }
    public static void swap(int[][] preswap, int i, int j){
        int[] temp = preswap[i];
        preswap[i] = preswap[j];
        preswap[j] = temp;

    }
    public static void destroy(int[][] donutline){
        for(int destroy = 0;destroy <donutline.length;destroy++){
            if(donutline[destroy].length == 1){
                if(donutline[destroy][0] == 0){
                    swap(donutline,destroy,donutline.length - 1);
                    int[][] newarray = new int[donutline.length - 1][];
                    int[][] temp;
                    for(int zo = 0; zo < newarray.length; zo++){
                        newarray[zo] = donutline[zo];
                    }
                    for(int x = 0; x <newarray.length;x++){
                        System.out.println(Arrays.toString(newarray[x]));
                    }
                }
            }
        }
    }
    public static int arraycompare(int[] johnson, int[] richard){
        if(richard.length <= 1 || johnson.length <= 1){
            if(johnson[0] < richard[0]){
                return -1;
            }
            else if(johnson[0] > richard[0]){
                return 1;
            }
            else{
                 return 0;
            }
        }
        else{
            if(johnson[0] < richard[0]){
                return -1;
            }
            else if(johnson[0] > richard[0]){
                return 1;
            }
            else{
                if(johnson.length > 1 && richard.length > 1){
                    int[] johnson1 = new int[johnson.length - 1];
                    int[] richard1 = new int[richard.length - 1];
                    for(int y = 0; y < richard1.length; y++){
                        richard1[y] = richard[y+1];
                    }
                    for(int x = 0; x < johnson1.length; x++){
                        johnson1[x] = johnson[x+1];
                    }
                    return arraycompare(johnson1,richard1);
                }
                else{
                    return 0;
                }
            }
        }
    }
    public static void sort(int[][] donutline){
        for(int sort = 0;sort < donutline.length; sort++){
            for(int sortee = sort + 1;sortee <donutline.length; sortee++){
                if(donutline[sort][0] > donutline[sortee][0]){
                    swap(donutline,sort,sortee);
                }
                else if(donutline[sort][0] == donutline[sortee][0]){
                    if(arraycompare(donutline[sort],donutline[sortee]) > 0){
                        swap(donutline,sort,sortee);
                        }
                    else if(arraycompare(donutline[sort],donutline[sortee]) == 0){
                        if(donutline[sortee].length < donutline[sort].length){
                            swap(donutline,sort,sortee);
                        }
                    }
                }
            }
        }
    }
}

