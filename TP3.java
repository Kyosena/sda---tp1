import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.util.*;
import java.io.IOException;
import java.lang.Math;


public class TP3{
    public static void main(String[] args)throws IOException{
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter write = new BufferedWriter(new OutputStreamWriter(System.out));
        String[] candidates = read.readLine().split(" ");
        Tree treeko = new Tree();
        HashMap<String,Node<Long>> Nodes = new HashMap<String,Node<Long>>();
        HashMap<String,Integer> candidatevotes = new HashMap<String,Integer>();
        for(int x = 0; x < 2;x++){
            candidatevotes.put(candidates[x],x);
        }
        int y = Integer.parseInt(read.readLine());
        for(int z = 0; z < y; z++){
            if(z == 0){
                String[] words = read.readLine().split(" ");
                Node root = new Node(words[0]);
                treeko.root = root;
                Nodes.put("root",root);
                for(int a = 2; a < words.length; a++){
                    String name = words[a];
                    Nodes.put(name,new Node(name)); 
                    Nodes.get("root").addchild(Nodes.get(name));
                }
            }
            else{
                String[] words = read.readLine().split(" ");
                for(int b = 2;b <words.length; b++){
                    String name = words[b];
                    Nodes.put(name,new Node(name));
                    Nodes.get(words[0]).addchild(Nodes.get(name));
                }
            }

        }
        int z0 = Integer.parseInt(read.readLine());
        for(int a1 = 0; a1 < z0; a1++){
            String[] commands = read.readLine().split(" ");
            if(commands[0].equals("TAMBAH")){
                String key = commands[1];
                long voter1 = Long.parseLong(commands[2]);
                long voter2 = Long.parseLong(commands[3]);
                Nodes.get(key).addvote(voter1, voter2);
                // Nodes.get(key).getParent().count();
            }
            else if(commands[0].equals("ANULIR")){
                String key = commands[1];
                long voter1 = Long.parseLong(commands[2]);
                long voter2 = Long.parseLong(commands[3]);
                Nodes.get(key).subvote(voter1, voter2);
                // Nodes.get(key).getParent().count();
            }
            else if(commands[0].equals("CEK_SUARA")){
                String key = commands[1];
                if(Nodes.get(key) != null){
                    write.write(Long.toString(Nodes.get(key).getVote1()) + " " + Long.toString(Nodes.get(key).getVote2()));
                }
                else{
                    write.write(Long.toString(Nodes.get("root").getVote1()) + " " + Long.toString(Nodes.get("root").getVote2()));
                }
                write.newLine();
            }
            else if(commands[0].equals("WILAYAH_MENANG")){
                int counter = 0;
                String key = commands[1];
                int candidate = candidatevotes.get(key);
                if(candidate == 0){
                    for(String key0 : Nodes.keySet()){
                        if(Nodes.get(key0).getVote1() > Nodes.get(key0).getVote2()){
                            counter++;
                        }
                    }
                }
                else{
                    for(String key0 : Nodes.keySet()){
                        if(Nodes.get(key0).getVote2() > Nodes.get(key0).getVote1()){
                            counter++;
                        }
                    }
                }
                write.write(Long.toString(counter));
                write.newLine();
            }
            else if(commands[0].equals("CEK_SUARA_PROVINSI")){
                  ArrayList<Node<Long>> printer = Nodes.get("root").getChildren();
                  for(int x = 0; x < printer.size(); x++){
                      write.write(printer.get(x).id + " " + printer.get(x).getVote1() + " " + printer.get(x).getVote2());
                      write.newLine();
                  }
            }
            else if(commands[0].equals("WILAYAH_MINIMAL")){
                String key = commands[1];
                int counter = 0;
                int candidate = candidatevotes.get(key);
                long wanted = Long.parseLong(commands[2]);
                double math = wanted;
                double target = math/100;
                double percentage;
                for(String key0 : Nodes.keySet()){
                    Node x = Nodes.get(key0);
                    if(x.getVote1() == 0 && x.getVote2() == 0){
                        percentage = 50.0/100;
                    }
                    else if(candidate == 0){
                        double nilai = (double)x.getVote1();
                        percentage = (double) nilai / (double) x.totalvote;
                        // System.out.println(percentage);
                    }
                    else{
                        double nilai =(double) x.getVote2();
                        // System.out.println ("vote kandidat B dari wilayah " + x.id + " adalah " + x.getVote2());
                        // System.out.println("total vote dari wilayah " + x.id + " adalah " + x.totalvote);
                        percentage = (double) nilai / (double) x.totalvote;
                        // System.out.println(percentage);
                    }
                    if (percentage >= target) counter++;
                }
                write.write(Long.toString(counter));
                write.newLine();
            }
            else if(commands[0].equals("WILAYAH_SELISIH")){
                long target = Long.parseLong(commands[1]);
                int counter = 0;
                for(String key0 : Nodes.keySet()){
                    long difference = Math.abs(Nodes.get(key0).getVote1() - Nodes.get(key0).getVote2());
                    if(difference >= target ){
                        counter++;
                    }
                }
                write.write(Long.toString(counter));
                write.newLine();
            }
        }
        write.flush();
        // for(String key : Nodes.keySet()){
        //     Node x = Nodes.get(key);
        //     System.out.println(x.id + "," + x.vote1 + "," + x.vote2);
        // }
    }
    public static class Tree<Long>{
        public Node<Long> root;
        public Tree(){
            this.root = null;
        }

        public Tree(Node<Long> root){
            this.root = root;
        }
        public void print(Node<Long> root){
            System.out.println(root.id);
            if(root.children.size() != 0){
                for(int p = 0; p < root.children.size();p++){
                    root.children.get(p).print();
                }
            }
        }
        public boolean checkempty(){
            if(root != null){
                System.out.println("ada root");
                if(root.children.size() != 0){
                    System.out.println("ada anak");
                    return false;
                }
                else{
                    System.out.println("gak ada anak");
                    return true;
                }
            }
            return true;
        }
    }
    public static class Node<Long>{
        String id;
        long vote1;
        long vote2;
        long totalvote;
        Node<Long> parent;
        ArrayList<Node<Long>> children = new ArrayList<Node<Long>>();
        public Node(String x){
            this.id = x;
            this.vote1 =  0L;
            this.vote2 = 0L;
            this.totalvote = 0L;
        }
        public void printparent(){
            if(children.size() != 0){
                for(int x = 0; x < children.size(); x++){
                    children.get(x).printparent();
                }
            }   
            if(this.parent == null) System.out.println("mentok = " + this.id);
            else{
            System.out.println(this.id + "'s parent = " + this.parent.id);
            }
        }
        
        public void print(){
            System.out.println(this.id);
            if(children.size() != 0){
                for(int x = 0; x < children.size();x++)
                    children.get(x).print();
            }
        }
        public void addchild(Node<Long> child){
            child.setParent(this);
            this.children.add(child);
        }
        public ArrayList<Node<Long>> getChildren(){
            return this.children;
        } 
        public long getVote1(){
            return vote1;
        }
        public long getVote2(){
            return vote2;
        }
        public void setParent(Node<Long> calonortu){
            this.parent = calonortu;
        }
        
        public void addvote(long x, long y){
            this.vote1 += x;
            this.vote2 += y;
            this.totalvote = this.vote1 + this.vote2;
            if(this.parent != null){
                this.parent.addvote(x,y);
            }
        }
        public void subvote(long x, long y){
            this.vote1 -= x;
            this.vote2 -= y;
            this.totalvote = this.vote1 + this.vote2;
            if(this.parent != null){
                this.parent.subvote(x,y);
            }
        }
        public Node<Long> getParent(){
            return this.parent;
        }

    }
}


